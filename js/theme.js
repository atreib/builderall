/*
	theme.js - André Treib
		- JS do tema utilizado
*/

/*
	JQuery para capacidade de dropdown
*/
$(document).on("mouseover", ".dropdown", function (ev) {
	$(this).find(".dropdown-content").css("display", "block");
	if ($(this).hasClass("inner-row")) {
		$(this).find(".dropdown-content").css("position", "fixed");
	}
});

$(document).on("mouseout", ".dropdown", function (ev) {
	$(this).find(".dropdown-content").css("display", "none");
});

