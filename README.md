Listagem de websites
====================

Linguagens e bibliotecas
---------------------

O layout foi desenvolvido usando HTML, CSS e Bootstrap, a fim de aproveitar regras que seriam �teis para o projeto. Para aperfei�oar de acordo com a necessiade, algumas regras foram reescritas, tais quais as cores do tema.

Para melhorar a usabilidade e criar alguns efeitos considerados necess�rios, foi utilizado JQuery.

Para consultar e listar os dados, foi utilizado AngularJS. Criei uma vari�vel que cont�m um JSON com um exemplo de listagem que poderia vir via API. Pelo foco ser o layout, n�o foram implementadas fun��es tal qual filtro.

Detalhes
--------------------

O layout � responsivo e foi planejado para uma visualiza��o mobile tamb�m. Por n�o poder fugir do estilo de lista, escolhi por esconder alguns bot�es, campos e colunas no layout mobile e utilizar bot�es dropdown ao inv�s.

> Andr� Treib, 2019.